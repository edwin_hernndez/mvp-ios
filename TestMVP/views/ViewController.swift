//
//  ViewController.swift
//  TestMVP
//
//  Created by Edwin Hernández Garduño on 8/20/19.
//  Copyright © 2019 Edwin Hernández Garduño. All rights reserved.
//

import UIKit

class ViewController: UIViewController, TrafficLightViewDelegate {

    @IBOutlet weak var txtDescription: UILabel!
    
    private let trafficLightPresenter = TrafficLightPresenter(trafficLightService: TrafficLightService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        trafficLightPresenter.setViewDelegate(trafficLightViewDelegate: self)
    }

    @IBAction func RedLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName: "Rojo")
    }
    
    @IBAction func YellowLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName: "Amarillo")
    }
    
    @IBAction func GreenLightAction(_ sender: Any) {
        trafficLightPresenter.trafficLightColorSelected(colorName: "Verde")
    }
    
    func displayTrafficLight(description:(String)) {
        txtDescription.text = description
    }
}
