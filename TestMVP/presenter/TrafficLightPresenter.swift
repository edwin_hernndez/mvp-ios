//
//  File.swift
//  TestMVP
//
//  Created by Edwin Hernández Garduño on 8/20/19.
//  Copyright © 2019 Edwin Hernández Garduño. All rights reserved.
//

import Foundation

protocol TrafficLightViewDelegate: NSObjectProtocol {
    func displayTrafficLight(description: (String))
}

class TrafficLightPresenter {
    
    private let trafficLightService: TrafficLightService
    weak private var trafficLightViewDelegate: TrafficLightViewDelegate?
    
    init ( trafficLightService: TrafficLightService){
        self.trafficLightService = trafficLightService
    }
    
    func setViewDelegate(trafficLightViewDelegate: TrafficLightViewDelegate?){
        self.trafficLightViewDelegate = trafficLightViewDelegate
    }
    
    func trafficLightColorSelected(colorName:(String)){
       
        trafficLightService.getTrafficLight(colorName: colorName) {[weak self] trafficLight in
            
            if let trafficLight = trafficLight {
                self?.trafficLightViewDelegate?.displayTrafficLight(description: trafficLight.description)
            }
        }
        }
}
