//
//  TrafficLightService.swift
//  TestMVP
//
//  Created by Edwin Hernández Garduño on 8/20/19.
//  Copyright © 2019 Edwin Hernández Garduño. All rights reserved.
//

import Foundation

class TrafficLightService {
    
    func getTrafficLight(colorName: (String), callBack:(TrafficLight?) -> Void){
        
        let trafficLights = [TrafficLight(colorName: "Rojo", description: "Alto"),
        TrafficLight(colorName: "Amarillo", description: "Más rápido"),
        TrafficLight(colorName: "Verde", description: "Rápido")]
        
        
        if let foundTrafficLight = trafficLights.first(where: {$0.colorName == colorName}) {
            callBack(foundTrafficLight)
        } else {
            callBack(nil)
        }
    }
}
