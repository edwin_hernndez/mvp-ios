//
//  TrafficLight.swift
//  TestMVP
//
//  Created by Edwin Hernández Garduño on 8/20/19.
//  Copyright © 2019 Edwin Hernández Garduño. All rights reserved.
//

import Foundation

struct TrafficLight {
    let colorName: String
    let description: String
}
